// 车辆详情信息 使用示例
import MapCarInfo from "./index.vue"

function setTip(pick, pointType, EL, data) {
    // 使用示例
    new TipWindow({
        id: pick.id.id, // 这个ID是点击获取的pick对象的entity的id（具体实现请参考cesium官网或其他相关文档，关键字“cesium获取pick对象”）
        viewer: window.viewer, // viewer对象就是cesium的声明实例（具体实现请参考cesium官网或其他相关文档）
        tipType: pointType, // 与entity对应的pointType类（需要跟随的类，主要是对应PopupTips的key）
        position: pick.id.position._value, // pick对象的位置
        options: data, // data数据
        EL: EL, // 需要传输的EL组件
        theme: 'light', // 背景 默认 dark
    })
}
setTip(pick, 'ent-pointType', MapCarInfo, {id:'宁EA8888', name: '宁EA8888'})