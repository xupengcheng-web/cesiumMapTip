/*
 * @Author: “ChengNan” 1429323234@qq.com
 * @Date: 2023-05-09 14:02:44
 * @LastEditors: “ChengNan” 1429323234@qq.com
 * @LastEditTime: 2023-05-10 20:59:08
 * @FilePath: 
 * @Description: 地图跟随弹框核心js
 */

import Vue from "vue"
let WindowVm = null

export default class TipWindow {
    constructor(option) {
            console.log("TipWindow option", option, window.viewer)
            if(!(option.id)) {
                throw '缺少参数 id 属性'
            }
            if(!(option.EL)) {
                throw '缺少引入组件 EL 属性'
            }
            if(!(option.tipType)) {
                throw '缺少引入组件 tipType 属性'
            }
            WindowVm = Vue.extend(option.EL)
            this.id = option.id
            this.viewer =  option.viewer || window.viewer;
            this.tipType =  option.tipType;
            this.position = option.position;
            this.vmInstance = new WindowVm({
                propsData: {
                    theme: option.theme,
                    options: option.options
                }
            }).$mount(); //根据模板创建一个面板

            this.setPopupTips(this.id)
            

            //点击窗口上的关闭按钮
            this.vmInstance.$refs.TipWindow.closeEvent = e => {
                this.windowClose();
            };
            //点击窗口放在最前面
            this.vmInstance.$refs.TipWindow.clickEvent = e => {
                this.resizeWindow();
            };
            // 初始化位置
            this.initPosition();

            //将字符串模板生成的内容添加到DOM上
            this.viewer.cesiumWidget.container.appendChild(this.vmInstance.$el); 

            //添加场景事件
            this.addPostRender();
        }
    setPopupTips() {
        if(!window.PopupTips) {
            window.PopupTips = {}
        }
        if(!window.PopupTips[this.tipType]) {
            window.PopupTips[this.tipType] = []
        }

        window.PopupTips[this.tipType].push(this)
    }
        // 初始化位置，消除第一次显示的误差
    initPosition() {
        if (!this.vmInstance.$el || !this.vmInstance.$el.style) return;
        const canvasHeight = this.viewer.scene.canvas.clientHeight;
        const windowPosition = new Cesium.Cartesian2();
        Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene, this.position, windowPosition);
        this.vmInstance.$el.style.bottom = canvasHeight - windowPosition.y + 100 + "px";
        const elWidth = this.vmInstance.$el.offsetWidth;
        this.vmInstance.$el.style.left = windowPosition.x + (elWidth / 2) + "px";
        this.vmInstance.$el.style.zIndex = 1
    }

    //添加场景事件
    addPostRender() {
        this.viewer.scene.postRender.addEventListener(this.postRender, this);
    }

    //场景渲染事件 实时更新标签的位置 使其与笛卡尔坐标一致
    postRender() {
        if (!this.vmInstance.$el || !this.vmInstance.$el.style) return;
        const canvasHeight = this.viewer.scene.canvas.clientHeight;
        const windowPosition = new Cesium.Cartesian2();
        // let windowPosition = Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene, this.position)
        Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene, this.position, windowPosition);
        this.vmInstance.$el.style.bottom = canvasHeight - windowPosition.y + 100 + "px";
        const elWidth = this.vmInstance.$el.offsetWidth;
        this.vmInstance.$el.style.left = windowPosition.x - 30 + "px";
    }

    //关闭
    windowClose() {
        this.vmInstance.$el.remove()
        // 删除PopupTips对应的类
        if(window.PopupTips) {
            for (const type in  window.PopupTips) {
                for (let index = window.PopupTips[type].length - 1; index >= 0 ; index--) {
                    const element = window.PopupTips[type][index];
                    if(element.id === this.id) window.PopupTips[type].splice(index, 1)
                }
            }
        }
        this.viewer.scene.postRender.removeEventListener(this.postRender, this); //移除事件监听
    }
    resizeWindow() {
        if(window.PopupTips){
            for (const key in window.PopupTips) {
                for (const element of window.PopupTips[key]) {
                    console.log(element.vmInstance.$el.style);
                    if (element.vmInstance && 
                        element.vmInstance.$el && 
                        element.vmInstance.$el.style) element.vmInstance.$el.style.zIndex = 1
                }
            }
        }

        if (this.vmInstance && 
            this.vmInstance.$el && 
            this.vmInstance.$el.style) this.vmInstance.$el.style.zIndex = 2
    }
}